# Demo of multiplayer game.
# Player1 movement with KinematicBody2D and collisions using
# AWSD as control keys.
# Regular movement.
extends KinematicBody2D

var speed = 250
var velocity = Vector2()

func get_input():
	# Detect up/down/left/right keystate and only move when pressed.
	velocity = Vector2()
	if Input.is_action_pressed('player2_right'):
		velocity.x += 1
	if Input.is_action_pressed('player2_left'):
		velocity.x -= 1
	if Input.is_action_pressed('player2_down'):
		velocity.y += 1
	if Input.is_action_pressed('player2_up'):
		velocity.y -= 1
	velocity = velocity.normalized() * speed

func _physics_process(delta):
	get_input()
	move_and_collide(velocity * delta)
	
