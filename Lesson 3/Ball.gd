extends RigidBody2D
	
# Apply force only when a KinematicBody2D hits the ball.
func _on_Ball_body_entered(body: Node) -> void:
	if (body.is_class("KinematicBody2D")):
		var current_velocity = get_linear_velocity()
		if (current_velocity.x < 2000 && current_velocity.x > -2000):
			apply_impulse(Vector2(), current_velocity * 0.7)
