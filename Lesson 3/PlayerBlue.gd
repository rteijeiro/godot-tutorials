# Demo of multiplayer game.
# Player2 movement with KinematicBody2D and collisions using
# default arrows as control keys.
# Smooth movement using linear interpolation.
extends KinematicBody2D

export var speed = 1000
export var friction = 0.2
export var acceleration = 1

var velocity = Vector2()

func get_input():
	var input = Vector2()
	if Input.is_action_pressed('ui_right'):
		input.x += 1
	if Input.is_action_pressed('ui_left'):
		input.x -= 1
	if Input.is_action_pressed('ui_down'):
		input.y += 1
	if Input.is_action_pressed('ui_up'):
		input.y -= 1
	return input

func _physics_process(delta):
	var direction = get_input()
	if direction.length() > 0:
		velocity = lerp(velocity, direction.normalized() * speed, acceleration)
	else:
		velocity = lerp(velocity, Vector2.ZERO, friction)
	velocity = move_and_slide(velocity)
