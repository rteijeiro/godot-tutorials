"""
Player Script
"""
extends KinematicBody2D

export var speed = 300
export var friction = 0.1
export var acceleration = 0.1
export var health = 200

var velocity = Vector2()
var laserScene = preload("res://Laser.tscn")
onready var explosion = $AnimatedSprite
var exploding = false

# Signals
signal died

func _ready() -> void:
	_restart_player()

func get_input():
	var input = Vector2()
	if Input.is_action_pressed('ui_right'):
		rotation = deg2rad(90)
		input.x += 1
	if Input.is_action_pressed('ui_left'):
		rotation = deg2rad(270)
		input.x -= 1
	if Input.is_action_pressed('ui_down'):
		rotation = deg2rad(180)
		input.y += 1
	if Input.is_action_pressed('ui_up'):
		rotation = deg2rad(0)
		input.y -= 1
	if Input.is_action_just_released("ui_accept"):
		laser_fire()
		
	return input
	
func _physics_process(delta):
	if !exploding:
		ship_move()
	
func ship_move():
	var direction = get_input()
	if direction.length() > 0:
		velocity = lerp(velocity, direction.normalized() * speed, acceleration)
	else:
		velocity = lerp(velocity, Vector2.ZERO, friction)
	velocity = move_and_slide(velocity)

func laser_fire():
	var left_laser = laserScene.instance()
	var right_laser = laserScene.instance()
	left_laser.global_position = $LeftLaser.global_position
	right_laser.global_position = $RightLaser.global_position
	get_tree().current_scene.add_child(left_laser)
	get_tree().current_scene.add_child(right_laser)	
	left_laser.move_laser(self.rotation)
	right_laser.move_laser(self.rotation)
	
	# Play laser sound
	$LaserAudioPlayer.play()

func get_damage(damage):
	if !exploding:
		health -= damage
		if health <= 200 && health > 100:
			set_modulate(Color(0, 1, 0))
		elif health <=100 && health > 50:
			set_modulate(Color(1, 0, 0))
		elif (health <= 0):
			set_modulate(Color(1, 1, 1))
			explosion.show()
			explosion.play()
			exploding = true
			
			# Play explosion sound
			$ExplosionAudioPlayer.play()

func _on_AnimatedSprite_animation_finished() -> void:
	emit_signal("died")
	_restart_player()
	
func _restart_player():
	exploding = false
	explosion.hide()
	health = 200
