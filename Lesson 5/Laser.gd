extends Area2D
	
func _physics_process(delta: float) -> void:
	if (rotation_degrees == 0):
		position.y -= 10
	elif (rotation_degrees == 90):
		position.x += 10
	elif (rotation_degrees == -90) || (rotation_degrees == 270):
		position.x -= 10
	elif (rotation_degrees < -179) || (rotation_degrees == 180):
		position.y += 10

func _on_Laser_body_entered(body: Node) -> void:
	if (body.is_in_group("asteroids")):
		self.queue_free()
		body.get_damage(10)

func _on_VisibilityNotifier2D_screen_exited() -> void:
	queue_free()

func move_laser(player_rotation: float) -> void:
	rotation = player_rotation
