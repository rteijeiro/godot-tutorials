extends RigidBody2D

onready var explosion = $AnimatedSprite

export var health = 100
var exploding = false

func _ready() -> void:
	randomize()
	explosion.hide()
	
func _physics_process(delta: float) -> void:
	if !exploding:
		apply_impulse(Vector2.ZERO, Vector2(rand_range(-100, 100), rand_range(-100, 100)))

func get_damage(damage):
	health -= damage
	if health <= 100 && health > 50:
		set_modulate(Color(0, 1, 0, 1))
	elif health <= 50 && health > 30:
		set_modulate(Color(0, 1, 1, 1))
	elif health <= 30 && health > 0:
		set_modulate(Color(1, 0, 0, 1))
	elif health <= 0:
		explosion.show()
		explosion.play()
		set_modulate(Color(1, 1, 1))
		exploding = true
		
		# Play explosion sound
		$ExplosionAudioPlayer.play()

func _on_AnimatedSprite_animation_finished() -> void:
	queue_free()

func _on_Asteroid_body_shape_entered(body_id: int, body: Node, body_shape: int, local_shape: int) -> void:
	if (body.name == "Player"):
		body.get_damage(50)
