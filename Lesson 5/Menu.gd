extends Control

onready var Game = preload("res://Game.tscn")

onready var ship = $Ship

var move_right = true
var move_left = false

func _ready() -> void:
	randomize()
	ship.position = Vector2(-50, 400)
	ship.visible = true
	
func _on_Start_pressed() -> void:
	get_tree().change_scene("res://Game.tscn")

func _process(delta: float) -> void:
	if (ship.position.x <= 1080) && move_right:
		ship.position.x += 10
		if (ship.position.x >= 1080):
			move_left = true
			move_right = false
		else:
			move_right = true
	elif (ship.position.x >= -50) && move_left:
		ship.position.x -=10
		if (ship.position.x <= -50):
			move_left = false
			move_right = true
		else:
			move_left = true
		
	
