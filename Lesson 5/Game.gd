extends Node2D

export var lifes = 3

onready var life = get_node("Container/Lifes/Life")
onready var ship1 = get_node("GUI/VBoxContainer/Ship1")
onready var ship2 = get_node("GUI/VBoxContainer/Ship2")
onready var ship3 = get_node("GUI/VBoxContainer/Ship3")

func _ready() -> void:
	var player = get_tree().get_root().find_node("Player", true, false)
	player.connect("died", self, "_player_died")

func _player_died():
	lifes -= 1
	match lifes:
		0: get_tree().change_scene("res://Game_Over.tscn")
		1: ship1.visible = false
		2: ship2.visible = false
		3: ship3.visible = false
