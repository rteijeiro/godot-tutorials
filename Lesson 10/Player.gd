extends KinematicBody

export var gravity = Vector3.DOWN * 12  # strength of gravity Vector3.DOWN * 12
export var speed = 7  # movement speed
export var jump_speed = 10  # jump strength
export var spin = 0.1  # rotation speed

var velocity = Vector3.ZERO
var jump = false

func get_input():
	var vy = velocity.y
	velocity = Vector3()
	jump = false
	if Input.is_action_pressed("move_left"):
		velocity += transform.basis.y * speed
	if Input.is_action_pressed("move_right"):
		velocity += -transform.basis.y * speed
	if Input.is_action_pressed("move_up"):
		velocity += transform.basis.x * speed
	if Input.is_action_pressed("move_down"):
		velocity += -transform.basis.x * speed
	if Input.is_action_just_pressed("ui_accept"):
		jump = true
	velocity.y = vy

func _physics_process(delta):
	velocity += gravity * delta
	get_input()
	if jump and is_on_floor():
		velocity.y = jump_speed
	velocity = move_and_slide(velocity, Vector3.UP)

func _unhandled_input(event):
	if event is InputEventMouseMotion:
		if event.relative.x > 0:
			rotate_y(-lerp(0, spin, event.relative.x / 10))
		elif event.relative.x < 0:
			rotate_y(-lerp(0, spin, event.relative.x / 10))

