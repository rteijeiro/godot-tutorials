extends Spatial

var Door = preload("res://Door.tscn")

func _ready() -> void:
	randomize()
	var step : int = 10
	for i in range(10):
		# Generate a random boolean value to enable static body. 
		var static_door = randi() & 1
		# Generates a random position for the door.
		var position = Vector3(-3, 3, -20 + step)
		instanceDoor(static_door, position)
		
		# Generate a random boolean value to enable static body. 
		static_door = randi() & 1
		# Generates a random position for the door.
		position = Vector3(0, 3, -20 + step)
		instanceDoor(static_door, position)
		
		# Generate a random boolean value to enable static body. 
		static_door = randi() & 1
		# Generates a random position for the door.
		position = Vector3(3, 3, -20 + step)
		instanceDoor(static_door, position)
		
		step += 10

func instanceDoor(static_door : bool, position : Vector3) -> void:
	var door = Door.instance()
	
	if static_door:
		door.mode = RigidBody.MODE_STATIC
	
	door.translation = position
	self.add_child(door)
