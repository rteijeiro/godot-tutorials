extends KinematicBody2D

export var speed = 300
export var friction = 0.1
export var acceleration = 0.1

var velocity = Vector2()
const NewBall = preload("res://Ball.tscn")
onready var captured_ball = get_node("CapturedBall")
var has_ball = false
var new_ball = null

func _ready() -> void:
	captured_ball.visible = false
	new_ball = instance_new_ball()
	
func instance_new_ball() -> RigidBody2D:
	var new_instance = NewBall.instance()
	new_instance.name = "Ball"
	new_instance.set_collision_layer_bit(1, 2)
	new_instance.set_collision_mask_bit(1, 2)
	return new_instance

	
func get_input():
	var input = Vector2()
	if Input.is_action_pressed('player2_right'):
		rotation = deg2rad(0)
		input.x += 1
	if Input.is_action_pressed('player2_left'):
		rotation = deg2rad(180)
		input.x -= 1
	if Input.is_action_pressed('player2_down'):
		input.y += 1
	if Input.is_action_pressed('player2_up'):
		input.y -= 1
	if Input.is_action_pressed("player2_kick") && has_ball:
		captured_ball.visible = false
		has_ball = false
		new_ball.global_position = get_node("Area2D/Feet").global_position
		get_tree().current_scene.add_child(new_ball)
		# new_ball.apply_impulse(global_position, global_position * 1000)
		
	return input

func _physics_process(delta):
	# Smooth player movement.
	var direction = get_input()
	if direction.length() > 0:
		velocity = lerp(velocity, direction.normalized() * speed, acceleration)
	else:
		velocity = lerp(velocity, Vector2.ZERO, friction)
	captured_ball.visible = true
	velocity = move_and_slide(velocity)

func _on_Area2D_body_entered(body: Node) -> void:
	if (body.name == new_ball.name) && !has_ball:
		print (has_ball)
		new_ball.queue_free()
		new_ball = instance_new_ball()
		captured_ball.visible = true
		has_ball = true
