extends KinematicBody2D

export var speed = 300
export var friction = 0.1
export var acceleration = 0.1

var velocity = Vector2()
var ball = null
var has_ball = false

func get_input():
	var input = Vector2()
	if Input.is_action_pressed('ui_right'):
		rotation = deg2rad(0)
		input.x += 1
	if Input.is_action_pressed('ui_left'):
		rotation = deg2rad(180)
		input.x -= 1
	if Input.is_action_pressed('ui_down'):
		input.y += 1
	if Input.is_action_pressed('ui_up'):
		input.y -= 1
	if Input.is_action_pressed("ui_accept") && has_ball:
		ball.sleeping = false
		ball.apply_impulse(Vector2(0, 0), velocity * 3)
		has_ball = false
		
	return input

func _physics_process(delta):
	var direction = get_input()
	if direction.length() > 0:
		velocity = lerp(velocity, direction.normalized() * speed, acceleration)
	else:
		velocity = lerp(velocity, Vector2.ZERO, friction)
	velocity = move_and_slide(velocity)

func _on_Area2D_body_entered(body: Node) -> void:
	if (body.name == "Ball"):
		body.sleeping = true
		ball = body
		has_ball = true
