extends KinematicBody2D

var Bullet = preload("res://Bullet.tscn")
export var speed = 200
var velocity = Vector2()

func get_input():
	# Add these actions in Project Settings -> Input Map.
	velocity = Vector2()
	if Input.is_action_pressed("move_down"):
		velocity = Vector2(-speed, 0).rotated(rotation)
	if Input.is_action_pressed("move_up"):
		velocity = Vector2(speed, 0).rotated(rotation)
	if Input.is_action_just_pressed('mouse_click'):
		shoot()

func shoot():
	# "Muzzle" is a Position2D placed at the barrel of the surfboard.
	var b = Bullet.instance()
	b.start($Muzzle.global_position, rotation)
	get_parent().add_child(b)

func _physics_process(_delta):
	get_input()
	var dir = get_global_mouse_position() - self.global_position
	# Don't move if too close to the mouse pointer.
	if dir.length() > 5:
		self.rotation = dir.angle()
		velocity = move_and_slide(velocity)

