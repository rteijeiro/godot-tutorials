# Demo of Multiplayer game
# Player1 movement with KinematicBody2D with collission
# and default arrow keys control.
extends KinematicBody2D

# Declare speed property.
export var speed = 250

# Declare velocity variable with default value Vector Zero.
var velocity = Vector2(0, 0)

# Get player input options.
func get_input():
	# Initialise velocity
	velocity = Vector2(0, 0)
	
	# Detect up/down/left/right keystate and only move when pressed.
	if Input.is_action_pressed('ui_right'):
		velocity.x += 1
	if Input.is_action_pressed('ui_left'):
		velocity.x -= 1
	if Input.is_action_pressed('ui_down'):
		velocity.y += 1
	if Input.is_action_pressed('ui_up'):
		velocity.y -= 1
	velocity = velocity.normalized() * speed

func _physics_process(delta):
	get_input()
	move_and_collide(velocity * delta)
